# Touch Config

Override every item's touch_interaction table according to user configuration.

## Configuration

The mode provides a configuration entry for each of the three components of the
touch_interaction table. Each can be set to one of three states:

1. don't override that entry

2. set entry to "long dig, short place"

3. set entry to "short dig, long place"

One can also choose not to override the 'hand' item. This isn't really useful except
as a simple test scenario giving different tools different touch_interaction values.