--[[
     This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
]]

local nothing = minetest.settings:get ("touch_interaction_override_nothing") or "none"
local node    = minetest.settings:get ("touch_interaction_override_node")    or "none"
local object  = minetest.settings:get ("touch_interaction_override_object")  or "none"

local new_nothing = nothing ~= "none" and nothing
local new_node    = node    ~= "none" and node
local new_object  = object  ~= "none" and object

local override_hand = minetest.settings:get_bool ("touch_interaction_override_hand", true)


local function do_override ()
	local count = 0

	for item, def in pairs (minetest.registered_items) do
		if item ~= "air" and item ~= "ignore" and item ~= "unknown"
			and (override_hand or item ~= "") then
			local t = def.touch_interaction or {}

			if new_nothing then t.pointed_nothing = new_nothing end
			if new_node    then t.pointed_node    = new_node    end
			if new_object  then t.pointed_object  = new_object  end
			minetest.override_item (item, {touch_interaction = t})

			count = count + 1
		end
	end

	minetest.log ("info", "[touch_interaction] processed " .. count .. " item definitions")
end


if new_nothing or new_node or new_object then
	minetest.register_on_mods_loaded (do_override)
else
	minetest.log ("warning", "[touch_interaction] nothing to do, check configuration")
end
